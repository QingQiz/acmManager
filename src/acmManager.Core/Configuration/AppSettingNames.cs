﻿namespace acmManager.Configuration
{
    public static class AppSettingNames
    {
        public const string UiTheme = "App.UiTheme";

        public const string CrawlerPath = "acmMgr.CrawlerPath";

        public const string PythonPath = "acmMgr.PythonPath";
    }
}
