﻿using acmManager.Article.Dto;

namespace acmManager.Contest.Dto
{
    public class SetContestResultInput : CreateArticleInput
    {
        public long Id { get; set; }
    }
}